﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EmailRegex
{
    public static class Validation
    {
        public static void EmailCount(string email)
        {
            Regex reg = new Regex(@"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+", RegexOptions.IgnoreCase);
            MatchCollection matches = reg.Matches(email);
            StringBuilder forEmail = new StringBuilder();
            foreach (Match match in matches)
            {
                while (!match.Success)
                {
                    Console.WriteLine("No match found");
                    Console.ReadLine();
                }
                forEmail.AppendLine(match.Value);
            }

            string emailLists = forEmail.ToString();
            Console.WriteLine(emailLists);
            Console.ReadLine();
        }
    }
}
